# Your Favicon Package

This package was generated with [RealFaviconGenerator](https://realfavicongenerator.net/) [v0.16](https://realfavicongenerator.net/change_log#v0.16)

## Install instructions

To install this package:

Extract this package in <code>https://f.eaglejump.org/</code>. For example, you should be able to access a file named <code>https://f.eaglejump.org/favicon.ico</code>.

Insert the following code in the `head` section of your pages:

    <link rel="apple-touch-icon" sizes="180x180" href="https://f.eaglejump.org/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="https://f.eaglejump.org/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="192x192" href="https://f.eaglejump.org/android-chrome-192x192.png">
    <link rel="icon" type="image/png" sizes="16x16" href="https://f.eaglejump.org/favicon-16x16.png">
    <link rel="manifest" href="https://f.eaglejump.org/site.webmanifest">
    <link rel="mask-icon" href="https://f.eaglejump.org/safari-pinned-tab.svg" color="#f52626">
    <link rel="shortcut icon" href="https://f.eaglejump.org/favicon.ico">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="https://f.eaglejump.org/mstile-144x144.png">
    <meta name="msapplication-config" content="https://f.eaglejump.org/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">

*Optional* - Check your favicon with the [favicon checker](https://realfavicongenerator.net/favicon_checker)